//
//  NewsletterCell.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 04/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit
import SnapKit

class NewsletterCell: UICollectionViewCell {

	// let headerView = HeaderCellView()

	override init(frame: CGRect) {
		super.init(frame: frame)
		backgroundColor = .whiteColor()

		let headerImageView: UIImageView = {
			let iv = UIImageView()
			iv.image = UIImage(named: "obrazek")
			iv.contentMode = .ScaleToFill
			iv.snp_makeConstraints(closure: { (make) in
				make.height.equalTo(UIScreen.mainScreen().bounds.size.width * 0.42)
				make.width.equalTo(UIScreen.mainScreen().bounds.size.width)

			})
			return iv
		}()

		let arrow: UIImageView = {
			let iv = UIImageView()
			iv.image = UIImage(named: "arrow")
			return iv
		}()

		let titleLabel: UILabel = {
			let label = UILabel()
			label.numberOfLines = 2
			label.font = UIFont.systemFontOfSize(18)
			label.text = "Poslední šance na nábytek Jitonahjkbnjkm! Poslední šance na nábytek Jitona!"

			return label
		}()

		let subtitleLabel: UILabel = {
			let label = UILabel()
			label.numberOfLines = 2
			label.font = UIFont.systemFontOfSize(12)
			label.textColor = .lightGrayColor()

			label.text = "Ulovte si posledních pár kousků za ceny, které se už nebudou opakovat"
			return label
		}()

//		let stack = UIStackView(arrangedSubviews: [headerImageView, titleLabel, subtitleLabel])
//		stack.axis = .Vertical
//
//		addSubview(stack)
//		stack.spacing = 20
//		stack.snp_makeConstraints { (make) in
//			make.top.equalTo(0)
//
//		}

		addSubview(headerImageView)

		addSubview(titleLabel)
		titleLabel.snp_makeConstraints { (make) in
			make.top.equalTo(headerImageView.snp_bottom).offset(10)
			make.leading.equalTo(headerImageView).offset(20)
			make.trailing.equalTo(headerImageView).offset(-50)
		}

		addSubview(subtitleLabel)
		subtitleLabel.snp_makeConstraints { (make) in
			make.top.equalTo(titleLabel.snp_bottom).offset(10)
			make.leading.equalTo(titleLabel)
			make.trailing.equalTo(titleLabel)
		}

		addSubview(arrow)
		arrow.snp_makeConstraints { (make) in
			make.height.equalTo(50)
			make.width.equalTo(50)
			make.top.equalTo(titleLabel.snp_bottomMargin)
			make.trailing.equalTo(headerImageView).offset(-10)

		}

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

}
