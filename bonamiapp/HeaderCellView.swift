//
//  HeaderCellView.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 19/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit
import SnapKit

class HeaderCellView: UIView {

	let stack = UIStackView()
//	let headerImageView = UIImageView()

	func viewDidLoad() {
		viewDidLoad()

		let headerImageView: UIImageView = {
			let iv = UIImageView()
			iv.image = UIImage(named: "testobrazek")
			return iv
		}()

		let arrow: UIImageView = {
			let iv = UIImageView()
			iv.image = UIImage(named: "arrow")
			return iv
		}()

		let titleLabel: UILabel = {
			let label = UILabel()
			label.numberOfLines = 2
			label.font = UIFont.boldSystemFontOfSize(20)
			label.text = "Poslední šance na nábytek Jitona!"
			return label
		}()

		let subtitleLabel: UILabel = {
			let label = UILabel()
			label.numberOfLines = 2
			label.textColor = .lightGrayColor()

			label.text = "Ulovte si posledních pár kousků za ceny, které se už nebudou opakovat"
			return label
		}()

		addSubview(stack)
		stack.snp_makeConstraints { (make) in
			make.height.equalTo(150)
			make.width.equalTo(UIScreen.mainScreen().bounds.size.width)
		}

		addSubview(headerImageView)
		headerImageView.snp_makeConstraints { (make) in
			make.height.equalTo(150)
			make.width.equalTo(UIScreen.mainScreen().bounds.size.width)
		}

		addSubview(titleLabel)
		titleLabel.snp_makeConstraints { (make) in
			make.top.equalTo(headerImageView.snp_bottom).offset(20)
			make.leading.equalTo(headerImageView).offset(20)
			make.trailing.equalTo(headerImageView).offset(-50)
		}

		addSubview(subtitleLabel)
		subtitleLabel.snp_makeConstraints { (make) in
			make.top.equalTo(titleLabel.snp_bottom).offset(10)
			make.leading.equalTo(titleLabel)
			make.trailing.equalTo(titleLabel)
		}

		addSubview(arrow)
		arrow.snp_makeConstraints { (make) in
			make.height.equalTo(50)
			make.width.equalTo(50)
			make.top.equalTo(titleLabel.snp_bottomMargin)
			make.trailing.equalTo(headerImageView).offset(-10)

		}

////	let stack = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
//	// stack.axis = .Vertical
//
//	self.addSubview(stack)
//	backgroundColor = .redColor()
//	stack.snp_makeConstraints { (make) in
//		make.leading.top.equalTo(120)
//		make.trailing.equalTo(-20)
//	}
//}

	}

}