//
//  BaseNewsletterCell.swift
//  bonamiapp
//
//  Created by Robert Varga on 27/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation
import UIKit

class BaseNewsletterCell : UICollectionViewCell {
    
    var imageView : UIImageView = {
        let container = UIImageView()
        container.backgroundColor = .redColor()
        return container
    }();
    
    var titleLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = UIColor.bonamiText90Black()
        label.font = UIFont.systemFontOfSize(20)
        return label
    }()
    
    var perexLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 5
        label.textColor = UIColor.bonamiText90Black()
        label.font = UIFont.systemFontOfSize(18)
        return label;
    }()
    var endsIn : UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = UIColor.whiteColor();
        label.backgroundColor = UIColor.bonamiText90Black().colorWithAlphaComponent(0.6);
        label.font = UIFont.systemFontOfSize(14)
        return label;
    }()
    var stockItemsLeft : UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = .whiteColor()
        label.backgroundColor = UIColor.bonamiRed().colorWithAlphaComponent(0.3);
        label.font = UIFont.systemFontOfSize(14)
        return label;
    }()
    var url : String = "";
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .whiteColor()
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOpacity = 0.15
        self.layer.shadowRadius = 2.0
        self.layer.shadowOffset = CGSizeMake(0, 0);
        self.clipsToBounds = true
        
        let cellWidth = self.bounds.size.width;
        addSubview(imageView);
        imageView.snp_makeConstraints{ make in
            make.leading.top.trailing.equalTo(0);
            make.height.equalTo(cellWidth / 2);
        }
        
        addSubview(titleLabel)
        titleLabel.snp_makeConstraints{ make in
            make.top.equalTo(imageView.snp_bottom).offset(10);
            make.leading.equalTo(5);
            make.trailing.equalTo(-50);
        }
        
        addSubview(perexLabel);
        perexLabel.snp_makeConstraints{make in
            make.top.equalTo(titleLabel.snp_bottom).offset(20);
            make.leading.equalTo(5);
            make.trailing.equalTo(-5)
        }
        
        addSubview(endsIn);
        endsIn.snp_makeConstraints{make in
            make.top.equalTo(imageView.snp_bottom).offset (-20);
            make.leading.equalTo(5);
        }
        
        addSubview(stockItemsLeft);
        stockItemsLeft.snp_makeConstraints{make in
            make.top.equalTo(imageView.snp_bottom).offset (-40);
            make.leading.equalTo(5);
            make.width.equalTo(100);
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}