//
//  MagazinController.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 14/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import Argo
import SDWebImage

let MagazinCellId = "MagazinCellId"

class MagazinController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

	var usedLocale = "cz";
	let url = "https://www.bonami.cz/mcc16/magazine-articles?domain=";
	var articles: [Article] = [] {
		didSet {
			collectionView?.reloadData()
		}
	}
	var currentArticle: Article?;
    //let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)

	override func viewDidLoad() {
		super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MagazinController.orientationDidChange(_:)), name: UIDeviceOrientationDidChangeNotification, object: nil)

		view.backgroundColor = UIColor(colorLiteralRed: 0.843, green: 0.843, blue: 0.843, alpha: 1.00)
		collectionView?.backgroundColor = UIColor(colorLiteralRed: 0.843, green: 0.843, blue: 0.843, alpha: 1.00)
		collectionView?.registerClass(MagazinCell.self, forCellWithReuseIdentifier: MagazinCellId)
		collectionView?.snp_makeConstraints(closure: { (make) in
			make.edges.equalTo(super.view).inset(UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0))
		})

		let headers = [
			"Accept-Language": "cz",
			"Accept": "application/json"
		]

		Alamofire.request(.GET, url + self.usedLocale, headers: headers).validate()
			.responseJSON { response in
				if let error = response.result.error {
					print(error)
				}
				guard let json = response.result.value else { return }
				self.reload(json)
		}
	}

	override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

		self.currentArticle = articles[indexPath.item];
		let articleController = ArticleController();
		articleController.url = self.currentArticle!.url;

		navigationController?.pushViewController(articleController, animated: false)
	}

	override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MagazinCellId, forIndexPath: indexPath) as! MagazinCell
		let model = articles[indexPath.row]

		cell.titleLabel.text = model.title

        let humanReadableDateLabel = DateUtils.getNiceBeginDate(model.publishedAt, locale:model.language.languageLocale())

		cell.published.text = humanReadableDateLabel
		cell.subtitleLabel.text = model.perex
        
        for tag in model.tags{
            if let text =  cell.tagLabels.text{
                if (text == ""){
                     cell.tagLabels.text =  text + " " + tag;
                }else{
                    cell.tagLabels.text =  text + " | " + tag;
                }
            }else{
                cell.tagLabels.text = "";
            }
        }

		let imageUrl = NSURL(string: model.image)
		if let url = imageUrl {
			cell.image.sd_setImageWithURL(url)
		}

		return cell
	}

	override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return articles.count
	}
    
     func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) {
             //landscape
             let newWidth = ((view.frame.width - 20) / 2 ) - 20;
             let newHeight = newWidth * 0.67 + 100
             return CGSizeMake(newWidth, newHeight)
        } else {
             //portrait
             return CGSizeMake(view.frame.width - 20, view.frame.width * 0.67 + 60)
        }
    }
    
    func reload (json:AnyObject) -> () {
        let models: Decoded<[Article]> = decode(json)
        if let error = models.error {
            print("Error while decoding magazine objects with message \(error)")
        }
        self.articles = models.value ?? []
        self.articles.sortInPlace({ $0.publishedAt > $1.publishedAt });
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func orientationDidChange(notification: NSNotification) {
        collectionView!.collectionViewLayout.invalidateLayout()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        collectionView!.collectionViewLayout.invalidateLayout()
    }
    
}

