//
//  MagazinModel.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 14/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation
import Curry
import Argo

struct Article {
    let id: Int
    let url: String
    let title: String
    let image: String
    let perex: String
    let language:ArticleLanguage;
    let tags:[String];
    let publishedAt:String;
    let author:String;
}

extension Article: Decodable {
    static func decode(json: JSON) -> Decoded<Article> {
        return curry(self.init)
            <^> json <| "id"
            <*> json <| "url"
            <*> json <| "title"
            <*> json <| "image"
            <*> json <| "perex"
            <*> json <| "language"
            <*> json <|| "tags"
            <*> json <| "publishedAt"
            <*> json <| "author"
    }
}

struct ArticleDetail {
	let content: String
}

extension ArticleDetail: Decodable {
	static func decode(json: JSON) -> Decoded<ArticleDetail> {
		return curry(self.init)
		<^> json <| "content"

	}
}

enum ArticleLanguage : String{
    case Czech = "cs"
    case Polish = "pl"
    case Slovak = "sk"
    
    func languageLocale() -> String{
        switch self {
            case .Czech: return "cs_CZ";
            case .Polish: return "pl_PL";
            case .Slovak: return "sk_SK";
        }
    }
}
extension ArticleLanguage:Decodable{}