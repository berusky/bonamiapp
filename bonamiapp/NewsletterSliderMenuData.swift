//
//  NewsletterSliderMenuData.swift
//  bonamiapp
//
//  Created by Robert Varga on 23/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation

class NewsletterSliderMenuData{
    var sliderMenuLabels:[String];
    var newsletterIds:[Int];
    var dateToIdsMap:[String:[Int]];
    var newsletterIdToDetailMap: [Int:NewsletterDetail] = [:]
    
    init(labels:[String], newsletterIds:[Int], newsletters:[String:[Int]]){
        self.sliderMenuLabels = labels;
        self.newsletterIds = newsletterIds;
        self.dateToIdsMap = newsletters;
    }
    
    func findIdsByName(name:String)->[Int]{
            for (label, arrayOfIds) in dateToIdsMap{
                if (name == label){
                    return arrayOfIds;
                }
            }
        return [];
    }
}