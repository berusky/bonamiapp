//
//  DateUtils.swift
//  bonamiapp
//
//  Created by Robert Varga on 24/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation

struct DateUtils {
    static func getNiceBeginDate(date:String, locale:String) -> String {
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "EEEE, dd.MMMM";
        formatter.locale = NSLocale.init(localeIdentifier: locale)
        
        let dayTimePeriodFormatter: NSDateFormatter = NSDateFormatter();
        dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ";
        dayTimePeriodFormatter.timeZone = NSTimeZone.localTimeZone();
        
        return formatter.stringFromDate(dayTimePeriodFormatter.dateFromString(date)!);
    }
    
    static func convertToNSDate(date:String) -> NSDate {
        
        let dayTimePeriodFormatter: NSDateFormatter = NSDateFormatter();
        dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ";
        dayTimePeriodFormatter.timeZone = NSTimeZone.localTimeZone();
        
        return dayTimePeriodFormatter.dateFromString(date)!;
    }
    
    static func convertToDateLeft (date:NSTimeInterval) -> String{
            let interval = Int(date)
            var hours = (interval/3600)
            let days = (hours/24)
            hours = hours % 24;
            return String(format: "Už jen %02d dní %02d hodin", days, hours)
    }
}
