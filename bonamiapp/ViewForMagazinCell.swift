//
//  ViewForMagazinCell.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 19/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit

class ViewForMagazinCell {

	let imageView: UIImageView = {
		let iv = UIImageView()
		iv.image = UIImage(named: "obrazek2")
		iv.layer.masksToBounds = true
		iv.contentMode = .ScaleAspectFill

		return iv
	}()

	let postTitle: UILabel = {
		let label = UILabel()
		label.font = UIFont.boldSystemFontOfSize(20)
		label.numberOfLines = 3
		label.adjustsFontSizeToFitWidth = true

		return label
	}()

}
