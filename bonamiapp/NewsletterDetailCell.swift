//
//  NewsletterDetailCell.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 22/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.

import UIKit
import SnapKit

class NewsletterDetailCell: UICollectionViewCell {

	override init(frame: CGRect) {
		super.init(frame: frame)

		backgroundColor = .whiteColor()

		let thumbImage = UIImage(named: "kreslo")
		let thumbImageView = UIImageView(image: thumbImage)
		thumbImageView.contentMode = .ScaleAspectFill

		let title = UILabel()
		title.text = "Skládací křeslo Karup Boogie, bílý rám/fialový potah"
		title.font = UIFont.systemFontOfSize(18)
		title.numberOfLines = 3

		let price = UILabel()
		price.text = "10 288 Kč"

		let newPrice = UILabel()
		newPrice.font = UIFont.boldSystemFontOfSize(20)
		newPrice.text = "7 999 Kč"

		let verticalStack = UIStackView(arrangedSubviews: [title, price, newPrice])
		verticalStack.axis = .Vertical
		verticalStack.spacing = 3

		let mainStack = UIStackView(arrangedSubviews: [thumbImageView, verticalStack])
		mainStack.axis = .Horizontal

		self.addSubview(mainStack)
		mainStack.snp_makeConstraints { (make) in

			make.leading.top.equalTo(10)
			make.trailing.equalTo(-20)
		}

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
