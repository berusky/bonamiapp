//
//  NewsletterDetail.swift
//  bonamiapp
//
//  Created by Robert Varga on 20/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation
import Curry
import Argo

struct Newsletter{
    var id:Int = 0;
    var publishedAt: String = "";
    var domain:Domain = Domain.Czech;
    var perexTitle:String = "";
}

extension Newsletter: Decodable {
    
    static func decode(json: JSON) -> Decoded<Newsletter> {
        return curry(self.init)
            <^> json <| "id"
            <*> json <| "publishedAt"
            <*> json <| "domain"
            <*> json <| "perexTitle"
    }
}

struct NewsletterDetail{
    var id:Int;
    var domain:Domain;
    var perexTitle: String;
    var perexBody:String;
    var publishedAt:String;
    var magazineArticle:Article?;
    var lovedProduct:ProductDetail?;
    var onStockProducts: StockProductDetail?;
    var endingCampaigns: [CampaignDetail]?;
    var newCampaigns: [NewCampaignDetail]?;
    
}

extension NewsletterDetail: Decodable {
    static func decode(json: JSON) -> Decoded<NewsletterDetail> {
        let s =  curry(self.init)
            <^> json <| "id"
            <*> json <| "domain"
            <*> json <| "perexTitle"
            <*> json <| "perexBody"
            <*> json <| "publishedAt"
        return s
            <*> json <|? "magazineArticle"
            <*> json <|? "lovedProduct"
            <*> json <|? "onStockProducts"
            <*> json <||? "endingCampaigns"
            <*> json <||? "newCampaigns"
    }
}

struct ProductDetail{
    var niceUrl:String;
    var url:String;
    var name:String;
    var description:String;
    var campaignEndAt:String;
    var priceInfo: PriceInfoDetail;
    var images:[ProductImageDetail];
}

extension ProductDetail: Decodable {
    static func decode(json: JSON) -> Decoded<ProductDetail> {
        return curry(self.init)
            <^> json <| "niceUrl"
            <*> json <| "url"
            <*> json <| "name"
            <*> json <| "description"
            <*> json <| "campaignEndAt"
            <*> json <| "priceInfo"
            <*> json <|| "images"
    }
}

struct PriceInfoDetail{
    var price: String;
    var currency: Currency;
    var retailPrice:String?;
}

extension PriceInfoDetail: Decodable {
    static func decode(json: JSON) -> Decoded<PriceInfoDetail> {
        return curry(self.init)
            <^> json <| "price"
            <*> json <| "currency"
            <*> json <|? "retailPrice"
        
    }
    
    private func toDefaultValue(value:String) -> Decoded<String>{
        let finalValue:String = value ?? "N/A";
        return Decoded<String>.fromOptional(finalValue);
    }

}

struct ProductImageDetail{
    var productDetailThumbnail: String;
    var productDetailThumbnailRetina: String;
    var productDetailThumbnailFull:String;
}

extension ProductImageDetail:Decodable{
    
    static func create(productDetailThumbnail:String, productDetailThumbnailRetina:String, productDetailThumbnailFull:String) -> ProductImageDetail{
        return ProductImageDetail(productDetailThumbnail:productDetailThumbnail,productDetailThumbnailRetina: productDetailThumbnailRetina, productDetailThumbnailFull:productDetailThumbnailFull);
    }
    
    //    static func create(first:String, second:String, third:String) -> ImageDetail{
    //        return ImageDetail(productDetailThumbnail:first, productDetailThumbnailRetina: second, productDetailThumbnailFull:third);
    //    }
    
    static func decode(json: JSON) -> Decoded<ProductImageDetail> {
        return curry(self.init)
            <^> json <| "productDetail-thumbnail"
            <*> json <| "productDetail-thumbnail-retina"
            <*> json <| "productDetail-thumbnail-full"
    }
}

struct StockProductDetail{
    var productCount:Int;
    var products: [ProductDetail];
}

extension StockProductDetail:Decodable{
    
    static func decode(json: JSON) -> Decoded<StockProductDetail> {
        return curry(self.init)
            <^> json <| "productCount"
            <*> json <|| "products"
    }
}

struct CampaignDetail{
    var niceUrl:String;
    var name:String;
    let endAt:String;
    var perex:String;
    var description:String;
    var authorTitle:String;
    var authorDescription:String;
    var productCount:Int;
    var url:String;
    var images:CampaignImageDetail;
}

extension CampaignDetail: Decodable{
    
    static func decode(json: JSON) -> Decoded<CampaignDetail> {
        let s = curry(self.init)
            <^> (json <| "niceUrl")
            <*> (json <| "name")
            <*> (json <| "endAt")
            <*> (json <| "perex")
            <*> (json <| "description")
        return s
            <*> json <| "authorTitle"
            <*> json <| "authorDescription"
            <*> json <| "productCount"
            <*> json <| "url"
            <*> json <| "images"
    }

    private func toTest(dateString: String) -> Decoded<String> {
        return .fromOptional(dateString);
    }
    
    private func toInt(defaultValue: Int = 0, number: String) -> Decoded<Int> {
        return .fromOptional(Int(number) ?? defaultValue)
    }
    
    internal func toNSDate(format: String, dateString: String) -> Decoded<NSDate> {
        return .fromOptional(NSDate());
    }
}

struct NewCampaignDetail{
    var type:NewCampaignType;
    var campaign: CampaignDetail;
}

extension NewCampaignDetail:Decodable{
    static func decode(json: JSON) -> Decoded<NewCampaignDetail> {
        return curry(self.init)
            <^> json <| "type"
            <*> json <| "campaign"
    }
}

struct CampaignImageDetail{
    var lowres:String;
    var main:String;
    var thumb:String;
    var retina:String;
    var author:String;
}

extension CampaignImageDetail: Decodable{
    static func create(lowres:String, main:String, thumb:String, retina:String, author:String) -> CampaignImageDetail{
        return CampaignImageDetail(lowres:lowres, main:main, thumb:thumb, retina:retina, author:author);
    }
    
    static func decode(json: JSON) -> Decoded<CampaignImageDetail> {
        return curry(self.init)
            <^> json <| "homepage-lowres"
            <*> json <| "homepage-main"
            <*> json <| "homepage-thumbnail"
            <*> json <| "homepage-retina"
            <*> json <| "author"
    }
}

enum NewCampaignType: String {
    case BigProductMultiple = "big-product-multiple"
    case BigProductSingle = "big-product-single"
    case BigAuthor = "big-author"
    case Small = "small"
}

extension NewCampaignType:Decodable{}

enum Currency:String {
    case CZK = "CZK"
    case PLN = "PLN"
    case EUR = "EUR"
    
    func displayName() -> String{
        switch self {
            case CZK: return "Kč";
            case PLN: return "Zł";
            case EUR: return "€";
        }
    }
}

extension Currency:Decodable{}

enum Domain : String{
    case Czech = "cz"
    case Polish = "pl"
    case Slovak = "sk"
    
    func languageLocale() -> String{
        switch self {
            case .Czech: return "cs_CZ";
            case .Polish: return "pl_PL";
            case .Slovak: return "sk_SK";
        }
    }
}
extension Domain:Decodable{}


