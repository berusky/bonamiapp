//
//  CampgainsController.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 20/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit
import Alamofire
import Argo
import Curry

private let reuseIdentifier = "Cell"

class CampgainsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

	var campgains: [Campgain] = [] {
		didSet {
			collectionView?.reloadData()
		}

	}

	override func viewDidLoad() {
		super.viewDidLoad()

		collectionView?.backgroundColor = .redColor()
		collectionView?.registerClass(CampgainsCell.self, forCellWithReuseIdentifier: reuseIdentifier)

		let reload: (AnyObject) -> () = { [weak self] json in
			let models: Decoded<[Campgain]> = decode(json)
			if let error = models.error {
				print(error)
			}
			self?.campgains = models.value ?? []
		}

		let headers = [
			"Accept-Language": "cz",
			"Accept": "application/json"
		]

		Alamofire.request(.GET, "https://www.bonami.cz/mcc16/campaigns", headers: headers).validate()
			.responseJSON { response in
				if let error = response.result.error {
					print(error)
				}
				guard let json = response.result.value else { return }
				print(json)
				reload(json)
		}
	}

	override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 3
	}

	override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		return collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)
	}

	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		return CGSizeMake(view.frame.width, 240)
	}
}

