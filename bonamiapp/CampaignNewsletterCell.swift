//
//  CampaignNewsletterCell.swift
//  bonamiapp
//
//  Created by Robert Varga on 28/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation
import UIKit

class CampaignNewsletterCell: BaseNewsletterCell{
    
    var type: NewsletterItem!
    var additionalData: Any!
    
    var boom : UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = UIColor.bonamiText90Black()
        label.font = UIFont.systemFontOfSize(20)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(boom);
        boom.snp_makeConstraints{
            make in
            make.top.equalTo(perexLabel.snp_bottom).offset(20);
        }
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}