//
//  BigProductMultipleCell.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 27/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit

class BigProductMultipleCell: UICollectionViewCell {

	let titleLabel = UILabel()
	let subtitleLabel = UILabel ()
	var tagLabels = UILabel();
	let published = UILabel()
	let category = UILabel()
	var image = UIImageView()

	override init(frame: CGRect) {
		super.init(frame: frame)

		backgroundColor = .whiteColor()

		titleLabel.numberOfLines = 3
		titleLabel.text = "Podmanivá kola Electra"
		titleLabel.font = UIFont.systemFontOfSize(16, weight: UIFontWeightMedium)

		subtitleLabel.numberOfLines = 4
		subtitleLabel.text = "Legendární jízdní kola ze slunné Kalifornie"
		subtitleLabel.textColor = .lightGrayColor()
		subtitleLabel.font = UIFont.init(name: "SanFranciscoDisplay", size: 11);

		image.contentMode = .ScaleAspectFill
		image.layer.masksToBounds = true
		image.backgroundColor = .redColor()
		self.addSubview(image)

		let newWidth = self.bounds.size.width;
		let newHeight = newWidth * 0.67;
		
		image.snp_makeConstraints { (make) in
            make.leading.trailing.top.equalTo(self.contentView).offset(0)
            make.height.equalTo(newHeight)
		}
		addSubview(titleLabel)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

}