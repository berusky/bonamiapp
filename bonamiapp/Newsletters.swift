//
//  Newsletters.swift
//  bonamiapp
//
//  Created by Robert Varga on 16/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation
import Curry
import Argo

struct Newsletters{
    var newsletters:[Newsletter];
}

extension Newsletters: Decodable {
    static func decode(json: JSON) -> Decoded<Newsletters> {
        return curry(self.init)
            <^> json <|| "newsletters"
    }
}