//
//  PagingController.swift
//  bonamiapp
//
//  Created by Robert Varga on 25/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation
import PagingMenuController
import Alamofire
import Argo
import Curry

class PagingController: UIViewController, PagingMenuControllerDelegate {
    
    var usedLocale = "sk";
    let url = "https://www.bonami.cz/mcc16/newsletters?domain=";
    let urlDetailTemplate = "https://www.bonami.cz/mcc16/newsletters/";
    
    var newsletterSliderData:NewsletterSliderMenuData?;
    var nuws: [Newsletter] = [];
    var nuwsHeaderMap:[String:[Int]] = [:];
    var nuwsMap: [Int:NewsletterDetail] = [:];
    var currentNuwsletter:NewsletterDetail?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.whiteColor()
        
        let headers = [
            "Accept-Language": "cs",
            "Accept": "application/json"
        ]
        
        Alamofire.request(.GET, url+usedLocale, headers: headers).validate()
            .responseJSON { response in
                if let error = response.result.error {
                    print("Error while retrieving data from \(self.url) with \(error)");
                }
                guard let json = response.result.value else { return }
                self.parseNewsletters(json);
                for newsletter in self.nuws {
                    Alamofire.request(.GET, self.urlDetailTemplate + String(newsletter.id), headers: headers).validate()
                        .responseJSON { response in
                            if let error = response.result.error {
                                print("Error while retrieving data from \(self.urlDetailTemplate + String(newsletter.id)) with \(error)");
                            }
                            guard let json = response.result.value else { return }
                            self.parseNewsletterDetail(json)
                            //the last newsletter has been read
                            if (self.nuwsMap.count == self.nuws.count){
                                self.currentNuwsletter = self.nuwsMap[self.nuws[0].id];
                                print ("Current nuwsletter is \(self.currentNuwsletter!.id):\(self.currentNuwsletter!.perexTitle)")
                                self.newsletterSliderData?.newsletterIdToDetailMap = self.nuwsMap;
                                self.prepareLayout();
                            }
                    }
                }
        }
        
    }
    
    func prepareLayout(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 15
        layout.scrollDirection = .Vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        
        var viewControllers:[NewsletterDetailController] = [];
        var currentId = 0;
        var currentIndex = 0;
        for header in self.newsletterSliderData!.sliderMenuLabels {
            let tempVc = NewsletterDetailController(collectionViewLayout: layout);
            tempVc.title = header;
            var newsletterId = self.newsletterSliderData?.dateToIdsMap[header]![0];
            if (newsletterId == currentId){
                currentIndex += 1;
                newsletterId = self.newsletterSliderData?.dateToIdsMap[header]![currentIndex];
            }else{
                currentIndex = 0;
                currentId = newsletterId!;
            }
            tempVc.model = self.newsletterSliderData?.newsletterIdToDetailMap[newsletterId!];
            viewControllers.append(tempVc);
            if (viewControllers.count > 0){
                break;
            }
            print ("Date \(header) valueid \(newsletterId)");
        }
        
        let options = PagingMenuOptions()
        options.menuHeight = 40
        options.menuDisplayMode = .Standard(widthMode: .Flexible, centerItem: true, scrollingMode: .PagingEnabled)
        
        let pagingMenuController = PagingMenuController(menuControllerTypes: viewControllers, options: options)
        pagingMenuController.view.frame.origin.y += 64
        pagingMenuController.view.frame.size.height -= 64
        pagingMenuController.delegate = self;
        
        addChildViewController(pagingMenuController)
        view.addSubview(pagingMenuController.view)
        pagingMenuController.didMoveToParentViewController(self)
    }
    
    func parseNewsletterDetail (json:AnyObject) -> (){
        let models: Decoded<NewsletterDetail> = decode(json)
        if let error = models.error {
            print(error)
        }
        self.nuwsMap[models.value!.id] = models.value;
    }
    
    func parseNewsletters (json:AnyObject) -> () {
        let models: Decoded<[Newsletter]> = decode(json)
        if let error = models.error {
            print(error)
        }
        self.nuws = models.value ?? []
        self.nuws.sortInPlace({
            if ($0.publishedAt > $1.publishedAt) {
                return true;
            } else if ($0.publishedAt < $1.publishedAt) {
                return false;
            }
            else {return $0.id > $1.id}
        })
        var labels:[String]=[];
        for newsletter in self.nuws {
            
            let humanReadableDateLabel = DateUtils.getNiceBeginDate(newsletter.publishedAt, locale:newsletter.domain.languageLocale())
            
            var arr:[Int]? = self.nuwsHeaderMap[humanReadableDateLabel]
            if (arr == nil){
                arr = [newsletter.id];
            }else{
                arr!.append(newsletter.id);
            }
            self.nuwsHeaderMap[humanReadableDateLabel] = arr
            
            if (labels.count == 0){
                labels.append(humanReadableDateLabel);
            } else if (labels[0] != humanReadableDateLabel){
                labels.append(humanReadableDateLabel);
            }
        }
        
        self.newsletterSliderData = NewsletterSliderMenuData(labels: labels, newsletterIds: (self.nuws.map({newsletter in newsletter.id})), newsletters: self.nuwsHeaderMap)
    }
}