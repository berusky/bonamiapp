//
//  CampgainsCell.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 20/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit

class CampgainsCell: UICollectionViewCell {

	override init(frame: CGRect) {
		super.init(frame: frame)
		backgroundColor = .whiteColor()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}