//
//ColorUtils.swift
//bonamiapp
//
//Created by Robert Varga on 03 / 05 / 16.
//Copyright © 2016 bonami.cz.All rights reserved.

import Foundation
import UIKit
import UIColor_Hex_Swift

extension UIColor {
	class func bonamiText90Black() -> UIColor {
		return UIColor(rgba: "#1a1a1a");
	}
	class func bonamiText60Black() -> UIColor {
		return UIColor(rgba: "#999999");
	}
	class func bonamiBorder15Black() -> UIColor {
		return UIColor(rgba: "#e5e5e5");
	}
	class func bonamiBlue() -> UIColor {
		return UIColor(rgba: "#0531c1");
	}
	class func bonamiCyclamen() -> UIColor {
		return UIColor(rgba: "#f93f68");
	}
	class func bonamiLemonday() -> UIColor {
		return UIColor(rgba: "#4cc1cd");
	}
	class func bonamiLightBlue() -> UIColor {
		return UIColor(rgba: "#3498db");
	}
	class func bonamiDarkBlue() -> UIColor {
		return UIColor(rgba: "#34495e");
	}
	class func bonamiEmerald() -> UIColor {
		return UIColor(rgba: "#37cd62");
	}
	class func bonamiGreyDisabled() -> UIColor {
		return UIColor(rgba: "#d3d3d3");
	}
	class func bonamiEmeraldDark() -> UIColor {
		return UIColor(rgba: "#27ae60");
	}
	class func bonamiEmeraldLight() -> UIColor {
		return UIColor(rgba: "#58d68d");
	}
	class func bonamiGreen() -> UIColor {
		return UIColor(rgba: "#0b9511");
	}
	class func bonamiMenthol() -> UIColor {
		return UIColor(rgba: "#1bac9c");
	}
	class func bonamiPink() -> UIColor {
		return UIColor(rgba: "#ff9aba");
	}
	class func bonamiPurple() -> UIColor {
		return UIColor(rgba: "#9b59b6");
	}
	class func bonamiRed() -> UIColor {
		return UIColor(rgba: "#ec2027");
	}
	class func bonamiYellow() -> UIColor {
		return UIColor(rgba: "#ffc310");
	}
	class func bonamiOrange() -> UIColor {
		return UIColor(rgba: "#ff5213");
	}
	class func bonamiBrightGreen() -> UIColor {
		return UIColor(rgba: "#0ab602");
	}
	class func bonamiBrightBlue() -> UIColor {
		return UIColor(rgba: "#0471f0");
	}
	class func bonamiBackground() -> UIColor {
		return UIColor(rgba: "#f0f0f0");
	}
}