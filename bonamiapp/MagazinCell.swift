//
//  MagazinCell.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 14/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit
import SnapKit

class MagazinCell: UICollectionViewCell {

	let titleLabel = UILabel()
	let subtitleLabel = UILabel ()
    var tagLabels = UILabel();
	let published = UILabel()
	let category = UILabel()
	var image = UIImageView()

	override init(frame: CGRect) {
		super.init(frame: frame)
        
		backgroundColor = UIColor.bonamiOrange().colorWithAlphaComponent(0.2);
        layer.cornerRadius = 10;

		titleLabel.numberOfLines = 3
        titleLabel.font = UIFont.systemFontOfSize(16, weight: UIFontWeightMedium)

		subtitleLabel.numberOfLines = 4
		subtitleLabel.textColor = .lightGrayColor()
        subtitleLabel.font = UIFont.init(name: "SanFranciscoDisplay", size: 11);

		published.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
		published.textColor = UIColor.whiteColor()
        published.font = titleLabel.font.fontWithSize(14)
        
        tagLabels.backgroundColor = UIColor.bonamiBlue().colorWithAlphaComponent(0.6)
        tagLabels.textColor = .whiteColor()
        tagLabels.font = tagLabels.font.fontWithSize(12)
        tagLabels.numberOfLines = 2

		image.contentMode = .ScaleAspectFill
		image.layer.masksToBounds = true
		self.addSubview(image)
        
        let newWidth = self.bounds.size.width;
        let newHeight = newWidth * 0.67;
        let additionalLeftOffset = 5;

	image.snp_makeConstraints { (make) in
        make.leading.trailing.top.equalTo(self.contentView).offset(0)
        make.height.equalTo(newHeight)
	}
    
        self.addSubview(titleLabel);
        titleLabel.snp_makeConstraints { make in
            make.width.equalTo(newWidth - 50)
            make.top.equalTo(image.snp_bottom).offset(10);
            make.left.equalTo(image.snp_left).offset(additionalLeftOffset);
        }
		self.addSubview(published)
		published.snp_makeConstraints { (make) in
			make.top.equalTo(image.snp_bottom).offset(-60)
            make.left.equalTo(image.snp_left).offset(additionalLeftOffset);
		}
        
        self.addSubview(tagLabels)
        tagLabels.snp_makeConstraints { (make) in
            make.width.equalTo(newWidth - 50)
            make.top.equalTo(image.snp_bottom).offset(-40)
            make.left.equalTo(image.snp_left).offset(additionalLeftOffset);
        }
	}
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath){
            print (indexPath.item)
    }
    
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

}
