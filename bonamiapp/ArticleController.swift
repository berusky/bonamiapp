////
//  ArticleController.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 16/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON
import WebKit

class ArticleController: UIViewController, UIWebViewDelegate {

	var htmlData = ""
    var articleId:Int = 0;
	var url = "https://www.bonami.cz/mcc16/magazine-articles/"
    var webV = UIWebView();
    var spinnerViews:[UIView] = [];

	override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ArticleController.orientationDidChange(_:)), name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        webV = UIWebView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        webV.loadRequest(NSURLRequest(URL: NSURL(string: url)!))
        webV.delegate = self;
        webV.scalesPageToFit = true;
        self.view.addSubview(webV)
    }

    
    func webViewDidFinishLoad(webView: UIWebView) {
        webView.stringByEvaluatingJavaScriptFromString("var topBar = document.getElementsByClassName('main-head'); topBar[0].parentNode.removeChild(topBar[0]);var aside = document.getElementsByTagName('aside');aside[0].parentNode.removeChild(aside[0]); var related = document.getElementsByClassName('related-posts'); related[0].parentNode.removeChild(related[0]);var share = document.getElementsByClassName('tag-share'); share[0].parentNode.removeChild(share[0]);")
        
        for view in spinnerViews{
            view.hidden = true;
            view.removeFromSuperview()
        }
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        
        let image = UIImage.gifWithName("bonami-spinner");
        let imageView = UIImageView(frame:CGRectMake(100, 400, 60, 60));
        imageView.image = image;
        
        self.view.addSubview(imageView);
        
        imageView.snp_makeConstraints{ make in
            make.width.height.equalTo(50)
            make.center.equalTo(self.view)
        }

        spinnerViews.append(imageView);
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func orientationDidChange(notification: NSNotification) {
        self.webV.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height);
    }
    

}