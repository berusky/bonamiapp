//
//  CampgainsModel.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 20/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import Foundation
import Curry
import Argo

struct Campgain {
	let name: String
	let perex: String
	let endAt: String
	let productCount: Int
	let url: String
	let images: [Image]

}

extension Campgain: Decodable {
	static func decode(json: JSON) -> Decoded<Campgain> {
		return curry(self.init)
		<^> json <| "name"
		<*> json <| "perex"
		<*> json <| "endAt"
		<*> json <| "productCount"
		<*> json <| "url"
		<*> json <|| "images"

	}
}

struct Image {
	let homepage_lowres: String
	let homepage_main: String

}

extension Image: Decodable {
	static func decode(json: JSON) -> Decoded<Image> {
		return curry(self.init)
		<^> json <| "homepage-lowres"
		<*> json <| "homepage-main"

	}
}