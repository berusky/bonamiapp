//
//  CustomTabBarController.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 06/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {

    var deviceLocale:String="cz"{
        willSet(newLocale){
            if (newLocale == "cz" || newLocale == "sk" || newLocale == "pl"){
                usedLocale = newLocale;
            }else{
                usedLocale = "cz";
            }
        }
    }
    
    var usedLocale:String="cz";
    
	override func viewDidLoad() {
		super.viewDidLoad()
        
        let deviceLocaleLanguage = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode) as! String
        self.deviceLocale = deviceLocaleLanguage;
        
        let pagingController = PagingController();
        pagingController.navigationItem.title = "Denní inspirace"
        pagingController.usedLocale = self.usedLocale;
        let pagingNavController = UINavigationController (rootViewController: pagingController)
        pagingNavController.tabBarItem.title = "Inspirace"
        pagingNavController.tabBarItem.image = UIImage(named: "Inspirace")

        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 15
        layout.scrollDirection = .Vertical
        
        let magazineController = MagazinController (collectionViewLayout:layout);
        magazineController.navigationItem.title = "Magazín";
        magazineController.usedLocale = self.usedLocale;
        let magazineNavController = UINavigationController(rootViewController: magazineController)
        magazineNavController.tabBarItem.title = "Magazín"
        magazineNavController.tabBarItem.image = UIImage(named: "Magazin")
        
        viewControllers = [pagingNavController, magazineNavController];
	}

}
