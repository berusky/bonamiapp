//
//  NewsletterDetailController.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 22/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit

public enum NewsletterSection{
    case NewCampaigns
    case EndingCampaigns
    case OnStockItems
    case LovedProducts
    case Article
}

public enum NewsletterItem {
    case BigProductMultiple
    case BigProductSingle
    case Small
    case BigAuthor
    case OnStockItem
    case Article
    case LovedProduct
    case EndingCampaign
}

class NewsletterDetailController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let baseNewsletterCell = "baseNewsletterCell";
    let campaignNewsletterCell = "campaignNewsletterCell";
    
    var model: NewsletterDetail? {
        didSet{
            reloadData();
        }
    }
    
    var sections = [NewsletterSection]();
    var rows = [[NewsletterItem]]();
    
    func reloadData() -> (){
        sections = [NewsletterSection]()
        rows = [[NewsletterItem]]()
        
        if (model?.newCampaigns?.count > 0 ){
            sections.append(.NewCampaigns)
            var items = [NewsletterItem]();
            for campaign in (model?.newCampaigns)!{
                switch campaign.type {
                case NewCampaignType.BigAuthor:
                    items.append(.BigAuthor);
                case .BigProductMultiple:
                    items.append(.BigProductMultiple);
                case .BigProductSingle:
                    items.append(.BigProductSingle);
                default:
                    items.append(.Small);
                }
            }
            rows.append(items);
        }//end new campaigns
        
        //on stock products
        if (model?.onStockProducts?.products.count > 0){
             sections.append(.OnStockItems)
             var items = [NewsletterItem]();
             for _ in (model?.onStockProducts?.products)!{
                items.append(.OnStockItem);
             }
             rows.append(items);
        }
        
        //magazine article
        if (model?.magazineArticle != nil){
            sections.append(.Article)
            var items = [NewsletterItem]();
            items.append(.Article);
            rows.append(items);
        }
        
        //loved product
        if (model?.lovedProduct != nil){
            sections.append(.LovedProducts)
            var items = [NewsletterItem]();
            items.append(.LovedProduct);
            rows.append(items);
        }
        
        //ending campaigns
        if (model?.endingCampaigns?.count > 0){
            sections.append(.EndingCampaigns)
            var items = [NewsletterItem]();
            for _ in (model?.endingCampaigns)!{
                items.append(.EndingCampaign);
                rows.append(items);
            }
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        let section = rows[section];
        print ("section count \(section.count)");
        return section.count;
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        print ("sectionSS count \(sections.count)");
        return sections.count;
    }

	override func viewDidLoad() {
		super.viewDidLoad()

		collectionView?.backgroundColor = .lightGrayColor()

		view.backgroundColor = UIColor(colorLiteralRed: 0.843, green: 0.843, blue: 0.843, alpha: 1.00)
		collectionView?.translatesAutoresizingMaskIntoConstraints = false
		collectionView?.alwaysBounceVertical = true
		collectionView?.backgroundColor = UIColor(colorLiteralRed: 0.843, green: 0.843, blue: 0.843, alpha: 1.00)
		collectionView?.registerClass(BaseNewsletterCell.self, forCellWithReuseIdentifier: baseNewsletterCell)
        collectionView?.registerClass(CampaignNewsletterCell.self, forCellWithReuseIdentifier: campaignNewsletterCell)

	}

	override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var retCell: UICollectionViewCell!
        let rowType = rows[indexPath.section][indexPath.row];
        
        switch(rowType){
            case .BigProductMultiple, .BigProductSingle, .BigAuthor, .Small:
                retCell =  prepareNewCampaignCell(collectionView, newsletterCell: campaignNewsletterCell, indexPath: indexPath);
            
            case .LovedProduct:
                print ("loved product");
//                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(baseNewsletterCell, forIndexPath: indexPath) as! BaseNewsletterCell
//                retCell = cell;
            
            case .OnStockItem:
                print ("on stock product");
//                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(baseNewsletterCell, forIndexPath: indexPath) as! BaseNewsletterCell
//                retCell = cell;
            
            case .Article:
                print ("article");
//                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(baseNewsletterCell, forIndexPath: indexPath) as! BaseNewsletterCell
//                retCell = cell;
            
            case .EndingCampaign:
                print ("ending campaign");
//                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(baseNewsletterCell, forIndexPath: indexPath) as! BaseNewsletterCell
//                retCell = cell;
        }
        return retCell;
	}

    func prepareNewCampaignCell(collectionView:UICollectionView, newsletterCell:String, indexPath:NSIndexPath) -> UICollectionViewCell{
        print ("standard product");
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(newsletterCell, forIndexPath: indexPath) as! CampaignNewsletterCell
        let rowData = model?.newCampaigns![indexPath.row];
        
        cell.titleLabel.text = rowData?.campaign.name;
        cell.perexLabel.text = rowData?.campaign.perex;
        
        let imageUrl = NSURL(string: rowData!.campaign.images.retina)
        if let url = imageUrl {
            cell.imageView.sd_setImageWithURL(url)
        }
        
        let campaignEndAt = rowData?.campaign.endAt;
        let currentEndAtDate = DateUtils.convertToNSDate (campaignEndAt!);
        let currentDate = NSDate();
        
        cell.endsIn.text = DateUtils.convertToDateLeft(currentEndAtDate.timeIntervalSinceDate(currentDate));
        cell.stockItemsLeft.text = String(rowData!.campaign.productCount as! Int) + " kusů zbýva";
        cell.type = rows[indexPath.section][indexPath.row];
        cell.additionalData = rowData?.campaign;
        cell.boom.text = "I am too tired";
        
        return cell
    }
    
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		return CGSizeMake(view.frame.width, view.frame.height)
	}
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var url:String = "http://google.com";
        
        let rowType = rows[indexPath.section][indexPath.row];
        
        switch(rowType){
            case .BigProductMultiple, .BigProductSingle, .BigAuthor, .Small:
                url = (model?.newCampaigns![indexPath.row].campaign.url)!;
            case .LovedProduct:
                url = (model?.lovedProduct!.url)!;
            case .OnStockItem:
                url = (model?.onStockProducts?.products[indexPath.row].url)!;
            case .Article:
                url = (model?.magazineArticle?.url)!;
            case .EndingCampaign:
                url = (model?.endingCampaigns?[indexPath.row].url)!;
        }
        UIApplication.sharedApplication().openURL(NSURL(string: url)!)
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }

}
