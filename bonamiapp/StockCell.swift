//
//  StockCell.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 27/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//

import UIKit
import SnapKit

class StockCell: UICollectionViewCell {
	override init(frame: CGRect) {
		super.init(frame: frame)
		backgroundColor = .redColor()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

}

