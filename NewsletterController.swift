//
//  NewsletterVC.swift
//  bonamiapp
//
//  Created by Miroslav Šulák on 03/05/16.
//  Copyright © 2016 bonami.cz. All rights reserved.
//
import UIKit
import SnapKit
import Alamofire
import Argo

let cellId = "cellId"

class NewsletterControllerxx: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var usedLocale = "sk";
    let url = "https://www.bonami.cz/mcc16/newsletters?domain=";
    let urlDetailTemplate = "https://www.bonami.cz/mcc16/newsletters/";
    
    var newsletterSliderData:NewsletterSliderMenuData?;
    
    var nuws: [Newsletter] = [];
    
    var nuwsHeaderMap:[String:[Int]] = [:];
    var nuwsMap: [Int:NewsletterDetail] = [:];
    
    var currentNuwsletter:NewsletterDetail? {
        didSet{
            print("Reloading view with newsletter id \(currentNuwsletter?.id ?? -1)");
            collectionView?.reloadData();
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headers = [
            "Accept-Language": "cs",
            "Accept": "application/json"
        ]
        
        let parseNewsletters: (AnyObject) -> () = { [weak self] json in
            let models: Decoded<[Newsletter]> = decode(json)
            if let error = models.error {
                print(error)
            }
            self?.nuws = models.value ?? []
            self?.nuws.sortInPlace({
                if ($0.publishedAt > $1.publishedAt) {
                    return true;
                } else if ($0.publishedAt < $1.publishedAt) {
                    return false;
                }
                else {return $0.id > $1.id}
            })
            var labels:[String]=[];
            for newsletter in self!.nuws {
                
                let humanReadableDateLabel = DateUtils.getNiceBeginDate(newsletter.publishedAt, locale:newsletter.domain.languageLocale())
                
                var arr:[Int]? = self?.nuwsHeaderMap[humanReadableDateLabel]
                if (arr == nil){
                    arr = [newsletter.id];
                }else{
                    arr!.append(newsletter.id);
                }
                self?.nuwsHeaderMap[humanReadableDateLabel] = arr
     
                if (labels.count == 0){
                    labels.append(humanReadableDateLabel);
                } else if (labels[0] != humanReadableDateLabel){
                    labels.append(humanReadableDateLabel);
                }
            }
            
            self?.newsletterSliderData = NewsletterSliderMenuData(labels: labels, newsletterIds: (self?.nuws.map({newsletter in newsletter.id}))!, newsletters: self!.nuwsHeaderMap)
        }
        
        let parseNewsletterDetail: (AnyObject) -> () = { [weak self] json in
            let models: Decoded<NewsletterDetail> = decode(json)
            if let error = models.error {
                print(error)
            }
            self?.nuwsMap[models.value!.id] = models.value;
        }
        
        Alamofire.request(.GET, url+usedLocale, headers: headers).validate()
            .responseJSON { response in
                if let error = response.result.error {
                    print("Error while retrieving data from \(self.url) with \(error)");
                }
                guard let json = response.result.value else { return }
                parseNewsletters(json);
                for newsletter in self.nuws {
                    Alamofire.request(.GET, self.urlDetailTemplate + String(newsletter.id), headers: headers).validate()
                        .responseJSON { response in
                            if let error = response.result.error {
                                print("Error while retrieving data from \(self.urlDetailTemplate + String(newsletter.id)) with \(error)");
                            }
                            guard let json = response.result.value else { return }
                            parseNewsletterDetail(json)
                            //the last newsletter has been read
                            if (self.nuwsMap.count == self.nuws.count){
                                self.currentNuwsletter = self.nuwsMap[self.nuws[0].id];
                                print ("Current nuwsletter is \(self.currentNuwsletter!.id):\(self.currentNuwsletter!.perexTitle)")
                                self.newsletterSliderData?.newsletterIdToDetailMap = self.nuwsMap;
                            }
                    }
                }
        }
        
        view.backgroundColor = UIColor(colorLiteralRed: 0.843, green: 0.843, blue: 0.843, alpha: 1.00)
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = UIColor(colorLiteralRed: 0.843, green: 0.843, blue: 0.843, alpha: 1.00)
        collectionView?.registerClass(NewsletterCell.self, forCellWithReuseIdentifier: cellId)
        //		collectionView?.snp_makeConstraints(closure: { (make) in
        //			make.edges.equalTo(view).inset(UIEdgeInsetsMake(10, 10, 10, 10))
        //
        //		})
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("Commented out old way of going to blue chair menu");
//        let layout = UICollectionViewFlowLayout()
//        let newsletterDetailController = NewsletterDetailController(collectionViewLayout: layout)
//        navigationController?.pushViewController(newsletterDetailController, animated: true)
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCellWithReuseIdentifier(cellId, forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(view.frame.width, 240)
    }
}